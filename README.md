# Test exercise: Image gallery

## Features to implement
  - [x] Image grid from fetched http://jsonplaceholder.typicode.com/photos
  - [x] Pagination;
  - [x] Delete image
  - [x] Modal with image on list-item click
  - [x] Filtering by albumId (select or button list)

## Additional implementations
- [x] Sync pagination and filters with browser history

## Tech stack
- React
- Redux-toolkit
- React-router
- Axios
- Chakra-UI
- Framer-motion


## Result
- [Production deploy](https://test-exclusiveprs.netlify.app)
