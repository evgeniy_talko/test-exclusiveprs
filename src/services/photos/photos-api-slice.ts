import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { makeUrlSearchParamsString } from "../../utils/url";

export interface Photo {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

export interface ListResponse<T> {
  data: T[];
  totalPages: number;
  totalItems: number;
}

function providesList<R extends { id: string | number }[], T extends string>(
  resultsWithIds: R | undefined,
  tagType: T
) {
  return resultsWithIds
    ? [
        { type: tagType, id: "LIST" },
        ...resultsWithIds.map(({ id }) => ({ type: tagType, id })),
        tagType,
      ]
    : [{ type: tagType, id: "LIST" }];
}

export const apiSlice = createApi({
  reducerPath: "api",
  tagTypes: ["Photo", "Album"],
  baseQuery: fetchBaseQuery({
    baseUrl: "https://jsonplaceholder.typicode.com",
  }),

  endpoints(builder) {
    return {
      fetchPhotos: builder.query<
        ListResponse<Photo>,
        { page: number; limit: number; albumId?: number }
      >({
        providesTags: (results) => providesList(results?.data, "Photo"),
        query({ page = 1, limit = 10, albumId }) {
          return (
            "/photos" +
            makeUrlSearchParamsString([
              { _page: page },
              { _limit: limit },
              { albumId },
            ])
          );
        },
        transformResponse(response, meta, { limit = 10 }) {
          const totalItems: number | undefined =
            +meta?.response?.headers.get("X-Total-Count");
          const totalPages: number = Math.ceil(
            Number(totalItems) / Number(limit)
          );
          return { data: response, totalItems, totalPages };
        },
      }),

      deletePhotoById: builder.mutation({
        query: (imageId: number) => ({
          url: `/photos/${imageId}`,
          method: "DELETE",
        }),
        invalidatesTags: (_res, _err, imageId) => [
          { type: "Photo", id: imageId },
        ],
      }),

      fetchAlbums: builder.query({
        query: () => {
          return "/albums";
        },
        providesTags: (results) => providesList(results, "Album"),
      }),
    };
  },
});

export const {
  useFetchPhotosQuery,
  usePrefetch,
  useDeletePhotoByIdMutation,
  useFetchAlbumsQuery,
} = apiSlice;
