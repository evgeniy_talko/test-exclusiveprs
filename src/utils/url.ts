import { URLSearchParamsInit } from "react-router-dom";

export function makeParams(params: URLSearchParamsInit) {
  return new URLSearchParams(
    Array.from(params).filter(([_key, value]) => !!value)
  );
}

export function makeUrlSearchParamsString(
  array: { [key: string]: string | number | null }[]
) {
  const notEmpty = array.filter((object) => {
    return !!Object.values(object)[0];
  });

  const params = notEmpty
    .map((object) => {
      return Object.entries(object).map(([key, val]) => `${key}=${val}`);
    })
    .join("&");
  return "?" + params;
}
