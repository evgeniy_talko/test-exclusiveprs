import { ChakraProps } from "@chakra-ui/react";

const global = (_props: ChakraProps) => ({
  "html, body": {
    fontSize: "sm",
    margin: 0,
    padding: 0,
  },
  body: {
    backgroundColor: "grey.500",
    minHeight: "100vh",
  },
  "#root": {
    minHeight: "100vh",
    height: "auto",
  },
});

export default {
  global,
};
