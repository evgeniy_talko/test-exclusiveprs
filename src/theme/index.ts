import styles from "./styles";
import { extendTheme, theme as base } from "@chakra-ui/react";
const overrides = {
  styles,
  // borders,
  // Other foundational style overrides go here
  // components: {
  // Button,
  // Other components go here
  // },
};

export const theme = extendTheme(overrides);
