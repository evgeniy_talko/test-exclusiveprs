import React from "react";
import { DeleteIcon, ViewIcon } from "@chakra-ui/icons";
import {
  Text,
  Flex,
  LinkBox,
  LinkOverlay,
  Image,
  VStack,
  Button,
  HStack,
  useToast,
  UseToastOptions,
} from "@chakra-ui/react";
import {
  Photo,
  useDeletePhotoByIdMutation,
} from "../../../services/photos/photos-api-slice";
import { MotionBox } from "../../MotionBox";
import { sleep } from "../../../utils/time";

type Props = {
  image: Photo;
  setImage: React.ComponentState;
  openModal: () => void;
};

const ImageCard = ({ image, setImage, openModal }: Props) => {
  const [removed, setRemoved] = React.useState(false);
  const [hidden, setHidden] = React.useState(false);
  const [deleteImage, { isLoading }] = useDeletePhotoByIdMutation();
  const toast = useToast();

  const itemVariants = {
    hidden: {
      opacity: 0,
      scale: 0.98,
    },
    show: {
      opacity: hidden ? 0 : 1,
      scale: 1,
    },
  };

  const fakeDeleteResponse = async ({
    title,
    description,
    status = "info",
    position = "top",
    duration = 5000,
    isClosable = true,
  }: UseToastOptions) => {
    await sleep(1500);
    toast({
      title,
      description,
      position,
      status,
      duration,
      isClosable,
    });
    setHidden(true);
    await sleep(500);
    setRemoved(true);
  };

  const handleClick = () => {
    setImage(image);
    openModal();
  };

  const handleDelete = () => {
    deleteImage(image.id).unwrap();
    fakeDeleteResponse({
      title: "Product deleted",
      description: "We've just throws this product to the garbage 🙈. Really",
      status: "success",
    });
  };

  const maxHeight = "150px";

  return !removed ? (
    <MotionBox
      whileHover={{ y: -3 }}
      variants={itemVariants}
      transition={{ duration: 0.2, type: "tween", stiffness: 100 }}
      initial="hidden"
      exit="hidden"
      animate="show"
    >
      <LinkBox
        backgroundColor="white"
        as="article"
        boxShadow="md"
        transition="all"
        transitionDuration="1000"
        maxH={maxHeight}
        _hover={{
          boxShadow: "xl",
        }}
        rounded="md"
        overflow="hidden"
      >
        <Flex gap="3" justify="flex-start" maxH={maxHeight}>
          <Image flexShrink="0" src={image.thumbnailUrl} objectFit="contain" />
          <VStack
            justify="space-between"
            alignItems="start"
            py="4"
            pr="6"
            w="100%"
          >
            <LinkOverlay onClick={handleClick}>
              <Text fontSize="xl" noOfLines={2}>
                {image.title}
              </Text>
            </LinkOverlay>
            <HStack justify="end" w="100%">
              <Button leftIcon={<ViewIcon />} onClick={handleClick}>
                Show
              </Button>
              <Button
                onClick={handleDelete}
                variant="ghost"
                isLoading={isLoading}
                leftIcon={<DeleteIcon />}
              >
                Delete
              </Button>
            </HStack>
          </VStack>
        </Flex>
      </LinkBox>
    </MotionBox>
  ) : null;
};

export { ImageCard };
