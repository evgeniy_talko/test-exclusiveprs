import React, { ComponentState } from "react";
import {
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Spinner,
} from "@chakra-ui/react";
import { useFetchAlbumsQuery } from "../../../services/photos/photos-api-slice";
import { useNavigate, useSearchParams } from "react-router-dom";
import { makeParams } from "../../../utils/url";

const ImagesFilter: React.FC<{
  albumId: number | undefined;
  setAlbumId: ComponentState;
}> = ({ setAlbumId, albumId }) => {
  const { data, isLoading, isFetching } = useFetchAlbumsQuery(null);
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const changeAlbumId = (albumId: number) => {
    searchParams.set("albumId", albumId.toString());
    navigate(`?${makeParams(searchParams).toString()}`);
    setAlbumId(albumId);
  };

  if (isLoading || isFetching) return <Spinner size="md" />;

  return (
    <>
      <Menu>
        <MenuButton as={Button}>
          Filter by album {albumId > 0 ? `(${albumId})` : ""}
        </MenuButton>
        <MenuList maxH="400px" overflowY="auto">
          <MenuItem onClick={() => setAlbumId(null)}>All</MenuItem>
          {data.map((album: any) => {
            return (
              <MenuItem onClick={() => changeAlbumId(album.id)}>
                {album.id}
              </MenuItem>
            );
          })}
        </MenuList>
      </Menu>
    </>
  );
};

export { ImagesFilter };
