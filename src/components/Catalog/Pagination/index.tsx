import {
  useSearchParams,
  useLocation,
  NavLink,
  useNavigate,
} from "react-router-dom";
import { ArrowBackIcon, ArrowForwardIcon } from "@chakra-ui/icons";
import { Box, Button, HStack, Link } from "@chakra-ui/react";
import React from "react";
import { makeParams } from "../../../utils/url";
import { usePaginationContext } from "../../../context/pagination";

const Pagination: React.FC<{
  isFetching: boolean;
}> = ({ isFetching }) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const { setCurrentPage, currentPage, totalPages, isNext, isPrev } =
    usePaginationContext();

  const changePage = (diff: number) => {
    setCurrentPage((prev: number) => {
      const newPage = prev + diff;
      searchParams.set("_page", newPage.toString());
      navigate(`?${makeParams(searchParams).toString()}`);

      return newPage;
    });
  };

  return (
    <Box>
      <HStack>
        <Button
          isLoading={isFetching}
          leftIcon={<ArrowBackIcon />}
          onClick={() => changePage(-1)}
          disabled={!isPrev}
        >
          Prev
        </Button>

        <Button
          isLoading={isFetching}
          rightIcon={<ArrowForwardIcon />}
          onClick={() => changePage(1)}
          disabled={!isNext}
        >
          Next
        </Button>

        <Box fontSize="md">{`${currentPage} / ${totalPages}`}</Box>
      </HStack>
    </Box>
  );
};

export { Pagination };
