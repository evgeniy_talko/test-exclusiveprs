import {
  Heading,
  Text,
  HStack,
  Spinner,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import React from "react";
import { useSearchParams } from "react-router-dom";
import { PaginationProvider } from "../../context/pagination";
import {
  Photo,
  useFetchPhotosQuery,
} from "../../services/photos/photos-api-slice";
import { ImageGrid } from "../Catalog/ImageGrid";
import { ImageModal } from "../Catalog/ImageModal";
import { ImagesFilter } from "../Catalog/ImagesFilter";
import { PaginationPanel } from "../Catalog/PaginationPanel";

const Catalog: React.FC = () => {
  const [searchParams] = useSearchParams();
  const { onOpen, isOpen, onClose } = useDisclosure();
  const [currentPage, setCurrentPage] = React.useState<number>(
    +searchParams.get("_page") || 1
  );
  const [currentImage, setCurrentImage] = React.useState<Photo>();
  const [albumId, setAlbumId] = React.useState<number | undefined>(
    +searchParams.get("albumId")
  );
  const [limit, setLimit] = React.useState<number>(
    +searchParams.get("_limit") || 10
  );

  const {
    data: photos,
    error,
    isLoading,
    isFetching,
  } = useFetchPhotosQuery({
    page: currentPage,
    limit,
    albumId,
  });

  const paginationOptions = {
    totalItems: photos?.totalItems,
    currentPage,
    setCurrentPage,
    pageSize: limit,
    setPageSize: setLimit,
  };

  if (error) return <div>{error}</div>;
  if (isLoading) return <Spinner mt="14" size="xl" />;

  return (
    <VStack spacing="8" py="16" w="full">
      <HStack justify="space-between" w="full">
        <Heading as="h2">Product list</Heading>
        <Text fontSize="lg">Total products: {photos?.totalItems}</Text>

        <PaginationProvider config={paginationOptions}>
          <ImagesFilter albumId={albumId} setAlbumId={setAlbumId} />
        </PaginationProvider>
      </HStack>

      <ImageGrid
        setImage={setCurrentImage}
        openModal={onOpen}
        images={photos?.data}
      />

      <ImageModal
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        image={currentImage}
      />

      <PaginationProvider config={paginationOptions}>
        <PaginationPanel isFetching={isFetching} />
      </PaginationProvider>
    </VStack>
  );
};

export { Catalog };
