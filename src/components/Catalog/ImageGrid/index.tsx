import { AnimatePresence } from "framer-motion";
import React from "react";
import { Photo } from "../../../services/photos/photos-api-slice";
import { MotionGrid } from "../../MotionGrid";
import { ImageCard } from "../ImageCard";

type Props = {
  images: Photo[] | undefined;
  setImage: React.ComponentState;
  openModal: () => void;
};

function ImageGrid({ images, setImage, openModal }: Props) {
  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.15,
      },
    },
  };
  return (
    <>
      <MotionGrid
        variants={containerVariants}
        columns={[1, null, null, 2]}
        spacing="20px"
        initial="hidden"
        animate="show"
      >
        <AnimatePresence exitBeforeEnter>
          {images?.map((image) => (
            <ImageCard
              key={image.id}
              image={image}
              setImage={setImage}
              openModal={openModal}
            />
          ))}
        </AnimatePresence>
      </MotionGrid>
    </>
  );
}

export { ImageGrid };
