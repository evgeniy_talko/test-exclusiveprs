import React from "react";
import { HStack } from "@chakra-ui/react";
import { PageSizeSelect as PageSizeSelect } from "../PageSizeSelect";
import { Pagination } from "../Pagination";

const PaginationPanel = ({ isFetching }: { isFetching: boolean }) => {
  return (
    <HStack justify="space-between" w="100%">
      <Pagination isFetching={isFetching} />

      <PageSizeSelect />
    </HStack>
  );
};

export { PaginationPanel };
