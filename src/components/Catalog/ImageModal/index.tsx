import {
  Flex,
  Image,
  Modal,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import React from "react";
import { Photo } from "../../../services/photos/photos-api-slice";

const ImageModal: React.FC<{
  image: Photo;
  onOpen: () => void;
  isOpen: boolean;
  onClose: () => void;
}> = ({ image, isOpen, onClose }) => {
  if (!image) {
    return null;
  }
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay bg="blackAlpha.700" />
      <ModalContent boxShadow="2xl">
        <Flex h="100%" justify="center" alignItems="center">
          <Image src={image.url} alt={image.title} />
        </Flex>
        <ModalHeader>{image.title}</ModalHeader>
      </ModalContent>
    </Modal>
  );
};

export { ImageModal };
