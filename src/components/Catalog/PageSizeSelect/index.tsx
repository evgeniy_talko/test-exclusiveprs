import React from "react";
import { useSearchParams, useNavigate } from "react-router-dom";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { makeParams } from "../../../utils/url";
import { usePaginationContext } from "../../../context/pagination";

const PageSizeSelect: React.FC = () => {
  const perPageList = [10, 20, 30];
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const { pageSize, setPageSize } = usePaginationContext();

  const changePageSize = (pageSize: number) => {
    searchParams.set("_limit", pageSize.toString());
    navigate(`?${makeParams(searchParams).toString()}`);
    setPageSize(pageSize);
  };

  return (
    <Menu>
      <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
        {pageSize} items per page
      </MenuButton>
      <MenuList>
        {perPageList.map((pageSize, i) => (
          <MenuItem
            key={i}
            onClick={() => changePageSize(pageSize)}
            value={pageSize}
          >
            {pageSize}
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};

export { PageSizeSelect };
