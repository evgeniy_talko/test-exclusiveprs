import { SimpleGrid } from "@chakra-ui/react";
import { motion } from "framer-motion";

const MotionGrid = motion(SimpleGrid);
export { MotionGrid };
