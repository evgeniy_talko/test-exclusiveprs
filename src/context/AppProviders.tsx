import React from "react";
import { Provider } from "react-redux";
import { ChakraProvider } from "@chakra-ui/react";
import { store } from "../app/store";
import { theme } from "../theme";
import { BrowserRouter } from "react-router-dom";

const AppProviders: React.FC = ({ children }) => {
  return (
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <Provider store={store}>{children}</Provider>
      </BrowserRouter>
    </ChakraProvider>
  );
};

export { AppProviders };
