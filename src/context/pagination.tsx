import React, { ComponentState } from "react";
const PaginationContext = React.createContext<
  (Options & ComputedValues) | undefined
>(undefined);
PaginationContext.displayName = "PaginationContext";

type Options = {
  totalItems: number;
  currentPage: number;
  pageSize: number;
  setCurrentPage: ComponentState;
  setPageSize: ComponentState;
};

type ComputedValues = {
  totalPages: number;
  isNext: boolean;
  isPrev: boolean;
};

type Props = {
  config: Options;
  children: React.ReactNode;
};

function PaginationProvider({ config, children }: Props) {
  const { currentPage, totalItems, pageSize, setCurrentPage } = config;
  const totalPages: number = Math.ceil(totalItems / pageSize);
  const isNext = !(currentPage >= totalPages);
  const isPrev = !(currentPage === 1);

  if (currentPage > totalPages) setCurrentPage(1);

  return (
    <PaginationContext.Provider
      value={{ ...config, totalPages, isNext, isPrev }}
    >
      {children}
    </PaginationContext.Provider>
  );
}

function usePaginationContext() {
  const context = React.useContext(PaginationContext);
  if (context === undefined) {
    throw new Error(
      "'usePaginationContext' must be used within a <PaginationProvider />"
    );
  }
  return context;
}

export { usePaginationContext, PaginationProvider };
