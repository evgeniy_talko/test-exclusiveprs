import {
  Container,
  Box,
  Heading,
  Text,
  HStack,
  type ChakraProps,
} from "@chakra-ui/react";

export function AppContainer({
  children,
  ...props
}: { children: React.ReactNode } & ChakraProps) {
  return (
    <Container minH="full" maxW="container.lg" px="4" centerContent {...props}>
      {children}
    </Container>
  );
}

function MainLayout({ children }: { children: React.ReactNode }) {
  return (
    <Box h="full">
      <Box backgroundColor="gray.800" color="Menu">
        <AppContainer py="20px">
          <HStack spacing="6">
            <Text fontSize="6xl">🐨</Text>
            <Heading as="h1">Just another e-commerce site</Heading>
          </HStack>
        </AppContainer>
      </Box>

      <Box minH="100vh" backgroundColor="gray.200">
        <AppContainer flexGrow="1" backgroundColor="gray.50" minH="100vh">
          {children}
        </AppContainer>
      </Box>
    </Box>
  );
}

export { MainLayout };
