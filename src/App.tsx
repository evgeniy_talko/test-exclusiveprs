import { Catalog } from "./components/Catalog";
import { MainLayout } from "./layouts/MainLayout";

function App() {
  return (
    <MainLayout>
      <Catalog />
    </MainLayout>
  );
}

export default App;
